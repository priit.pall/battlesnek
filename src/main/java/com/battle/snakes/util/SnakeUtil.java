package com.battle.snakes.util;


import com.battle.snakes.game.*;

import java.util.*;
import java.util.stream.Collectors;

public class SnakeUtil {

  private static final Random RANDOM = new Random();

  public static MoveType getRandomMove(List<MoveType> possibleMoves) {
    /*
     * Given all possible moves, picks a random move
     * */
    return possibleMoves.get(RANDOM.nextInt(possibleMoves.size()));
  }

  public static boolean isInBounds(Board board, Coordinate coordinate) {
    /*
     * Given the game board, calculates if a coordinate is within the board
     * */
    return
            coordinate.getX() < board.getWidth() &&
            coordinate.getX() >= 0 &&
            coordinate.getY() < board.getHeight() &&
            coordinate.getY() >= 0;
  }

  public static Coordinate getNextMoveCoords(MoveType moveType, Coordinate start) {
    /*
     * Given the move type and the start coordinate, returns the coordinates of the next move
     * */
    switch (moveType) {
      case UP:
        return Coordinate.builder().x(start.getX()).y(start.getY() - 1).build();
      case DOWN:
        return Coordinate.builder().x(start.getX()).y(start.getY() + 1).build();
      case LEFT:
        return Coordinate.builder().x(start.getX() - 1).y(start.getY()).build();
      case RIGHT:
        return Coordinate.builder().x(start.getX() + 1).y(start.getY()).build();
      default:throw new IllegalArgumentException("Incorrect moveType value present");
    }
  }

  public static List<MoveType> getAllowedMoves(MoveRequest request) {
    /*
     * Given the move request, returns a list of all the moves that do not end in the snake dying
     * Hint: finding all the coordinates leading to the snakes death and
     * comparing it to the potential moves is a good starting point
     * */
    List<MoveType> allowedMoves = new ArrayList<>();
    List<Coordinate> tilesFilledWithSnakes = new ArrayList<>();
    for (Snake snake : request.getBoard().getSnakes()) {
      tilesFilledWithSnakes.addAll(snake.getBody());
    }

    Coordinate snakeHead = request.getYou().getBody().get(0);
    for (MoveType moveType : MoveType.values()) {
      Coordinate directionCoordinate = getNextMoveCoords(moveType, snakeHead);
      if (!tilesFilledWithSnakes.contains(directionCoordinate) && isInBounds(request.getBoard(), directionCoordinate)) {
        allowedMoves.add(moveType);
      }
    }
    return allowedMoves;
  }

  public static double getDistance(Coordinate first, Coordinate second) {
    /*
     * Given two coordinates on a 2D grid, calculates the distance between them
     * */
    double diffX = Math.pow(second.getX() - first.getX(), 2);
    double diffY = Math.pow(second.getY() - first.getY(), 2);
    return Math.sqrt(diffX + diffY);
  }

  public static MoveType getNearestMoveToTarget(Coordinate target, Coordinate current, List<MoveType> moves) {
    /*
     * Given the target coordinate, the current coordinate and a list of moves, returns
     * the nearest move to the target, selected from the moves list
     * */
    Optional<MoveType> closestMove = moves
            .stream()
            .min(Comparator.comparing(move -> getDistance(getNextMoveCoords(move, current), target)));
    if (closestMove.isPresent()) {
      return closestMove.get();
    }
    return MoveType.LEFT;
  }

  public static Coordinate getNearestCoordinateToTarget(Coordinate target, List<Coordinate> coords) {
    /*
     * Given the target coordinate and a list of coordinates, finds the nearest coordinate to the target
     * */
    return coords
            .stream()
            .min(Comparator.comparing(coordinate -> getDistance(coordinate, target)))
            .get();
  }

  /*
   * Given the move request check whether our snake is the biggest snake in the current game.
   * */
  public static boolean isBiggestSnakeOnBoard(MoveRequest moveRequest) {
    int ourSize = moveRequest.getYou().getBody().size();
    int enemyMaxSize = moveRequest.getBoard().getSnakes()
            .stream()
            .filter(snake -> !snake.equals(moveRequest.getYou()))
            .max(Comparator.comparing(snake -> snake.getBody().size()))
            .map(snake -> snake.getBody().size())
            .orElse(Integer.MAX_VALUE);

    return ourSize > enemyMaxSize;
  }

  public static List<Snake> getEdibleOpponents(MoveRequest moveRequest) {
    Snake you = moveRequest.getYou();
    List<Snake> edibleOpponents = moveRequest.getBoard().getSnakes()
            .stream()
            .filter(snake -> snake.getBody().size() < you.getBody().size())
            .sorted(Comparator.comparing(snake -> getDistance(you.getBody().get(0), snake.getBody().get(0))))
            .collect(Collectors.toList());

    return edibleOpponents;
  }

  /*
   * Given the move request, find the closes enemy snake which is smaller than you.
   * */
  public static Snake getClosestEdibleSnake(MoveRequest moveRequest) {
    Snake you = moveRequest.getYou();
    Snake closestSnake = null;
    double minDist = Double.MAX_VALUE;

    for (Snake snake : moveRequest.getBoard().getSnakes()) {
      double distance = getDistance(you.getBody().get(0), snake.getBody().get(0));
      if (distance < minDist) closestSnake = snake;
    }
    return closestSnake;
  }

  /*
   * Given the move request check whether the move after the current move would end up trapping the snake.
   * */
  public static List<MoveType> getAllowedMoveTypesNotResultingInDeadEnd(MoveRequest request) {
    List<MoveType> allowedMoves = getAllowedMoves(request);
    List<MoveType> result = new ArrayList<>();
    for (MoveType moveType : allowedMoves) {
      Coordinate head = request.getYou().getBody().get(0);
      Coordinate nextPosition = getNextMoveCoords(moveType, head);
      request.getYou().getBody().add(0, nextPosition);
      if (!getAllowedMoves(request).isEmpty()) {
        result.add(moveType);
      }
      request.getYou().getBody().remove(nextPosition);
    }
    return result;
  }

  /*
   * Given the move request and all possible move types, remove the move types which would make our snake get eaten
   * by bigger enemy snakes.
   * */
  public static List<MoveType> dontCollideWithBiggerSnakeHeads(MoveRequest moveRequest, List<MoveType> moveTypes) {
    List<Snake> enemies = moveRequest.getBoard().getSnakes();
    enemies.remove(moveRequest.getYou());
    Coordinate myHead = moveRequest.getYou().getBody().get(0);

    for (Snake snake : enemies) {
      if (snake.getBody().size() >= moveRequest.getYou().getBody().size()) {
        Coordinate enemyHead = snake.getBody().get(0);
        int dx = enemyHead.getX() - myHead.getX();
        int dy = enemyHead.getY() - myHead.getY();
        if (Math.abs(dx) == 1 && Math.abs(dy) == 1) {
          if (dx > 0) {
            moveTypes.remove(MoveType.RIGHT);
          } else if (dx < 0) {
            moveTypes.remove(MoveType.LEFT);
          }
          if (dy > 0) {
            moveTypes.remove(MoveType.DOWN);
          } else if (dy < 0) {
            moveTypes.remove(MoveType.UP);
          }
        } else if (Math.abs(dx) == 2 && dy == 0 || Math.abs(dy) == 2 && dx == 0) {
          if (dx == 2) {
            moveTypes.remove(MoveType.RIGHT);
          } else if (dx == -2) {
            moveTypes.remove(MoveType.LEFT);
          } else if (dy == 2) {
            moveTypes.remove(MoveType.DOWN);
          } else {
            moveTypes.remove(MoveType.UP);
          }
        }
      }
    }
    return moveTypes;
  }
}

For the 4th exercise I tried implementing the following logic:
1) Checking ahead
    - When having found all allowed moves, the system checks for each of these moves whether the snake
    would get into a dead end after taking said move.
2) Hunting enemy snakes
    - Our snake keeps track of other snakes. When our snake is the biggest snake, it checks for the closest
    enemy snake and tries to eat it.
3) Trying to avoid head-on collisions with snakes bigger than our's
    - Implemented check to try and avoid bigger snakes that are one step away from us.

Other more complex ideas which might be useful to make this snake even stronger:
* A* search
* Flood fill algorithm
* Some type of tail chasing algorithm
